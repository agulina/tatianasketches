module.exports = {
    title: 'Tatiana Sketches',
    titleTemplate: '%s 💎 Visual Merchandiser aspiring Graphic Designer',
    description: 'Follow my journey towards becoming a Graphic Designer',
    siteUrl: 'https://tatianasketches.com',
    image: '/social-card.png',
    icon: 'src/favicon.png',
    short_name: 'tatiana-sketches',
    owner: 'Tatiana Di Majo',
    instagramUrl: 'https://instagram.com/tatianasketches',
    instagramUsername: 'tatianasketches',
};
