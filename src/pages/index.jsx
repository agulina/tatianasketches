import React from 'react';
import { instagramUrl } from '../../config/website';
import Layout from '../components/Layout/Layout';
import SEO from '../components/SEO/SEO';

import styles from './main.module.scss';

const IndexPage = () => {
    return (
        <Layout>
            <SEO />
            <main>
                <section className={styles.illustrations}>
                    <article
                        className={`${styles.object} ${styles.objectBus}`}
                    />
                    <article
                        className={`${styles.object} ${styles.objectCoffee}`}
                    />
                    <article
                        className={`${styles.object} ${styles.objectIce}`}
                    />
                </section>
                <header className={styles.title}>
                    <h1>tatianasketches</h1>
                    <span>
                        <a
                            href={instagramUrl}
                            target="_blank"
                            rel="noopener noreferrer">
                            follow my journey
                        </a>
                    </span>
                </header>
            </main>
        </Layout>
    );
};

export default IndexPage;
