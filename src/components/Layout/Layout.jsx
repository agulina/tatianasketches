import React from 'react';
import Header from '../Header/Header';
import MetaLinks from '../MetaLinks/MetaLinks';

class Layout extends React.Component {
    render() {
        const { children } = this.props;
        return (
            <div>
                <MetaLinks />
                <Header />
                {children}
            </div>
        );
    }
}

export default Layout;
