import React, { Component } from 'react';
import { instagramUrl } from '../../../config/website';

import instagramIcon from '../../images/icons/instagram-round-white-24px.svg';
import styles from './header.module.scss';

class Header extends Component {
    render() {
        return (
            <header className={styles.header}>
                <nav className={styles.nav}>
                    <ul className={styles.navList}>
                        <li>
                            <a
                                href={instagramUrl}
                                target="_blank"
                                rel="noopener noreferrer"
                                className={styles.instagram}>
                                {/*<span className={styles.instagramText}>*/}
                                {/*    tatianasketches*/}
                                {/*</span>*/}
                                <img
                                    src={instagramIcon}
                                    width="18px"
                                    alt="Follow Tatiana sketches on Instagram"
                                />
                            </a>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;
