import React from 'react';
import Helmet from 'react-helmet';

const MetaLinks = () => {
    return (
        <>
            <Helmet>
                <html lang="en" />
                <meta httpEquiv="refresh" />
                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200&display=swap"
                    rel="stylesheet"
                />
            </Helmet>
        </>
    );
};

export default MetaLinks;
