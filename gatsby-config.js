const website = require('./config/website');

module.exports = {
    siteMetadata: {
        title: website.title,
        titleTemplate: website.titleTemplate,
        description: website.description,
        siteUrl: website.siteUrl,
        image: website.image,
        owner: website.owner,
    },
    plugins: [
        {
            resolve: 'gatsby-plugin-favicon',
            options: {
                background: '#102E56',
            },
        },
        {
            resolve: 'gatsby-plugin-sass',
            options: {
                includePaths: ['node_modules'],
            },
        },
        {
            resolve: 'gatsby-plugin-react-helmet',
        },
    ],
};
